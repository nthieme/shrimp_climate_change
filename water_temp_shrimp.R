###########################
####~~~~~~~~~~~~~~~~~~~####
###|GA water and shrimp|###
####~~~~~~~~~~~~~~~~~~~####
###########################

library(tidyverse)
library(readxl)
library(lubridate)
library(trend)
library(gridExtra)
library(grid)

####################
###Active section###
####################

###load in water data and do stats testing
setwd("~/Desktop/Datasets")
D <- read_xlsx("water_temp.xlsx")

year_ind<-which(nchar(D$`Row Labels`)==4)
D_temps<- data.frame()

for(i in 1:(length(year_ind)-1)){
  D_tmp<-data.frame(year =D[year_ind[i],]$`Row Labels`,  D[(year_ind[i]+1):(year_ind[(i+1)]-1),])
  D_temps<-rbind(D_tmp, D_temps)
}

D_temps_clean<-D_temps %>% mutate(month = case_when(
  Row.Labels=="1"~"Jan",
  Row.Labels=="2"~"Feb",
  Row.Labels=="3"~"Mar",
  Row.Labels=="4"~"Apr",
  Row.Labels=="5"~"May",
  Row.Labels=="6"~"Jun",
  Row.Labels=="7"~"Jul",
  Row.Labels=="8"~"Aug",
  Row.Labels=="9"~"Sept",
  Row.Labels=="10"~"Oct",
  Row.Labels=="11"~"Nov",
  Row.Labels=="12"~"Dec") %>% factor() %>% 
    fct_relevel("Jan", "Feb","Mar","Apr","May","Jun","Jul","Aug","Sept","Oct","Nov","Dec"),
  year = year %>% as.character %>% as.numeric,
  is.current = year == 2018,
  temp = Average.of.WTemp*(9/5)+32
) %>% select(year, month, temp, is.current ) %>% 
  mutate(  era = case_when(year == 2018~"2018",
                           year %>% between(1970, 1980)~"70s",
                           year %>% between(1980, 1990)~"80s",
                           year %>% between(1990, 2000)~"90s",
                           year %>% between(2000, 2010)~"00s",
                           year %>% between(2010, 2020)~"10s"),
           date_obj = str_c(month, year) %>% parse_date_time(., orders="b!-Y!")
  )

temps<-D_temps_clean %>% select(month, year, temp) %>% spread(month, temp) %>% gather(month, temp, -year) %>% 
  mutate(date_obj= str_c(month, year) %>% parse_date_time(., orders="b!-Y!")) %>% arrange(date_obj) %>% pull(temp)

##fixing na in temps data by using a sliding three month window
nas<-which(is.na(temps))
no_nas<-(1:length(temps))[-nas]

for(i in 1:length(nas)){
  wind_size<-1
  ma<-temps[(nas[i]-wind_size):(nas[i]+wind_size)] %>% na.omit
  
  if(length(ma)<2*wind_size){
    lower<-no_nas[which(no_nas<nas[i])] %>% max
    upper<-no_nas[which(no_nas>nas[i])] %>% min
    imputes <-seq(from=temps[lower], to = temps[upper], length.out = upper-lower+1)
    temps[lower:upper][-c(1, length(temps[lower:upper]))]<-imputes[-c(1, length(imputes))]
  }else{
    imputes<-mean(ma)
    temps[nas[i]]<-imputes
  }
}

###stats testing. we use a seasonal mann-kendall test to test for monotonicity of a time series. 
###H0: random, Ha: monotone

temp_ts<-ts(na.omit(temps), frequency = 12) 
test_res<-csmk.test(temp_ts)
sea.sens.slope(temp_ts)

###load in NASA SST data shown here: https://data.giss.nasa.gov/gistemp/graphs_v4/. data comes from ERSST
###need to format data to get it ready for plotting
D_sst_decadal <- read_csv("SST_decadal.csv") %>% mutate(year = str_sub(Year,1,4))%>% filter(year >= 1950)

years_decadal<-D_sst_decadal$Year %>% str_sub(1,4) %>% unique
months_col <- c()

for(i in 1:length(years_decadal)){
  mo<-D_sst_decadal %>% mutate(year = str_sub(Year,1,4)) %>% filter(str_sub(year, 1,4)==years_decadal[i]) %>% nrow
  mo_col<-1:mo
  months_col <- c(months_col, mo_col)
}

D_sst_decadal_f<-D_sst_decadal  %>% mutate(year = year %>% as.numeric()) %>% 
  add_column(months = months_col, source = "global") %>% 
  select(source, year, month = months, sst_a = Anomaly)

D_sst_decadal_f_1<-D_sst_decadal_f %>%filter(year>=1976)%>%  mutate(month = case_when(
  month=="1"~"Jan",
  month=="2"~"Feb",
  month=="3"~"Mar",
  month=="4"~"Apr",
  month=="5"~"May",
  month=="6"~"Jun",
  month=="7"~"Jul",
  month=="8"~"Aug",
  month=="9"~"Sept",
  month=="10"~"Oct",
  month=="11"~"Nov",
  month=="12"~"Dec") %>% factor() %>% 
    fct_relevel("Jan", "Feb","Mar","Apr","May","Jun","Jul","Aug","Sept","Oct","Nov","Dec"),
  year = year %>% as.character %>% as.numeric,
  is.current = year == 2018,
) %>% select(year, month, sst_a, is.current ) %>% 
  mutate(  era = case_when(year == 2018~"2018",
                           year %>% between(1970, 1980)~"70s",
                           year %>% between(1980, 1990)~"80s",
                           year %>% between(1990, 2000)~"90s",
                           year %>% between(2000, 2010)~"00s",
                           year %>% between(2010, 2020)~"10s"),
           date_obj = str_c(month, year) %>% parse_date_time(., orders="b!-Y!")
  )

###creating charts
sound_era<-D_temps_clean %>% group_by(era, month) %>%summarise(temp = mean(temp)) %>% ungroup %>% 
  mutate(era = era %>% fct_relevel("70s","80s","90s","00s","10s","2018")) %>% 
  ggplot(aes(x = month, y = temp, color=era, group = era))+geom_line(size=1.1, alpha = .75)+
  scale_color_manual(values = c("#d8d6dd", "#bdc9e1","#74a9cf", "#2b8cbe", "#045a8d", "#000000"),name = "Era")+
  theme_minimal()+theme(axis.text.x = element_text(angle = 45))+xlab("Month of the year")+
  ylab("Average water temperature (F)")+ggtitle("Georgia coast")

sound_mo<-D_temps_clean %>%ggplot(aes(x = date_obj, y = temp))+geom_line(color="#363655", alpha = .8)+theme_minimal()+
  geom_smooth()+xlab("Time")+ylab("Water temperatue (F)")

global_era<-D_sst_decadal_f_1 %>%group_by(era, month) %>%summarise(temp = mean(sst_a)) %>%ungroup %>%  
  mutate(era = era %>% fct_relevel("70s","80s","90s","00s","10s", "2018")) %>% 
  ggplot(aes(x = month, y = temp, color=era, group = era))+geom_line(size=1.1, alpha = .75)+
  scale_color_manual(
    values = c("#d8d6dd", "#bdc9e1","#74a9cf", "#2b8cbe", "#045a8d", "#000000"), name = "Era")+
  theme_minimal()+theme(axis.text.x = element_text(angle = 45))+xlab("Month of the year")+
  ylab("Average temperature anomaly")+ggtitle("Worldwide")

global_mo<-D_sst_decadal_f_1 %>% ggplot(aes(x = date_obj, y = sst_a))+geom_line(color="#363655", alpha = .8)+theme_minimal()+
  geom_smooth()+xlab("Time")+ylab("Water temperature anomaly")

grid.arrange(sound_era, global_era, sound_mo, global_mo, ncol = 2, 
             top =  textGrob("The water's getting hotter",gp=gpar(fontsize=15,font=1)))

###############
###GRAVEYARD###
###############

library("dCovTS")
library(httr)
library(rnoaa)
library(tidync)
library(numbers)

###there's actualy some cool stuff here

D_temps_clean %>% ggplot(aes(x = month, y = temp, color=year, group = year, alpha = is.current))+
  geom_line(size = 1)+scale_alpha_manual(values=c(.5, 1), guide="none")+
  scale_color_continuous(low="#e9edf2", high="#204872", name="Year")+theme_minimal()+
  theme(axis.text.x = element_text(angle = 45))+xlab("Month of the year")+ylab("Water temperature (F)")+
  ggtitle("Temperatures generally higher")

D_temps_clean %>% ggplot(aes(x = month, y = temp, color=year, group = year, alpha = is.current))+
  geom_line(size = 1)+scale_alpha_manual(values=c(.1, 1), guide="none")+
  scale_color_continuous(low="#e9edf2", high="#000000", guide = "none")+theme_minimal()+
  theme(axis.text.x = element_text(angle = 45))+xlab("Month of the year")+ylab("Water temperature (F)")+
  ggtitle("Temperatures generally higher")+labs(subtitle = "2018 in bold")


###getting ERSST data
###this is for the Wassaw, Ossabaw, Sapelo, St. Simon’s , St. Andrews, and Cumberland sounds 
years <- 1950:2019
months <- 1:12
D_sst <- tibble(year = numeric(), month = numeric(), sst = numeric())

for(month_i in months){
  for(year_i in years){
    if(month_i%in%c(11, 12)&year_i==2019){
      next
    }else{
      ersst_file<-ersst(year_i, month_i)
      
      sst_v<-tidync(ersst_file$filename) %>% # all sounds are in this grid cell
        hyper_filter(lat = between(lat, 33, 35), lon = between(lon, -80+358,-78+358)) %>% hyper_tibble %>% pull(sst)
      
      D_sst <- D_sst %>% add_row(year = year_i, month = month_i, sst = sst_v)
    } 
  }
}

D_sst %>% mutate(date_obj = parse_date_time(str_c(month, "-", year), "m!*-Y!")) %>% write_csv("SST_data_sounds.csv")

#time here is in units of days since 1800 citation:
#https://www.esrl.noaa.gov/psd/thredds/dodsC/Datasets/icoads/1degree/equatorial/std/sst.mean.nc.html

###just sst data
D_sst<-tidync("sst.mean.nc") %>% # all sounds are in this grid cell
  hyper_filter(lat = between(lat, 31, 32), lon = between(lon, -81+360,-80+360)) %>% hyper_tibble %>% 
  mutate(date_obj = as_date(time, origin = "1800-01-01")) %>% 
  mutate(sst=sst*(9/5)+32,year=year(date_obj), month = month(date_obj)) %>% select(year, month, date_obj, sst)

baseline_1951_1980<-D_sst%>% filter(between(year, 1951,1980)) %>% group_by(month) %>% 
  summarise(sst_base_sst = mean(sst, na.rm = TRUE)) 

D_sst_f <- D_sst %>% left_join(baseline_1951_1980) %>% 
  mutate(sst_a = sst-sst_base_sst, source = "sounds", year = year %>% as.numeric()) %>% group_by(year, month) %>% 
  summarise(source = source[1],sst_a = mean(sst_a), sst = mean(sst)) %>% select(source, year, month, sst_a)

D_temps_essrt<-bind_rows(D_sst_decadal_f, D_sst_f) %>% mutate(
  date_obj = parse_date_time(str_c(month,"-",year), "m!*-Y!"), 
  year=year %>% as.numeric(),
  era = case_when(
    year %>% between(1970, 1979)~"70s",
    year %>% between(1980, 1989)~"80s",
    year %>% between(1990, 1999)~"90s",
    year %>% between(2000, 2009)~"00s",
    year %>% between(2010, 2019)~"10s"),
  month = case_when(
    month=="1"~"Jan",
    month=="2"~"Feb",
    month=="3"~"Mar",
    month=="4"~"Apr",
    month=="5"~"May",
    month=="6"~"Jun",
    month=="7"~"Jul",
    month=="8"~"Aug",
    month=="9"~"Sept",
    month=="10"~"Oct",
    month=="11"~"Nov",
    month=="12"~"Dec") %>% 
    factor() %>% 
    fct_relevel("Jan", "Feb","Mar","Apr","May","Jun","Jul","Aug","Sept","Oct","Nov","Dec")
) %>% na.omit 

D_temps_essrt %>% filter(source == "sounds") %>%  group_by(era, month) %>%summarise(temp = mean(sst)) %>% 
  ungroup %>% mutate(era = era %>% fct_relevel( "70s","80s","90s","00s","10s")) %>% 
  ggplot(aes(x = month, y = temp, color=era, group = era))+geom_line(size=1.1, alpha = .75)+
  scale_color_manual(
    values = c("#d8d6dd","#a7a8c1", "#6b7fa8", "#476c9a","#045a8d"), name = "Era")+
  theme_minimal()+theme(axis.text.x = element_text(angle = 45))+xlab("Month of the year")+
  ylab("Average monthly water temperature (F)")+ggtitle("Temperatures generally higher")

#only a couple duplicate month/year/station, and they don't differ.
D_dnr<-read_xlsx("DNR_data.xlsx") %>% filter(str_sub(Station,1,1)!=6) %>% mutate(sst = WTemp*(9/5)+32) %>% 
  select(year = YEAR, month = MONTH, station = Station, sst)%>% group_by(year, month, station) %>% 
  summarise(sst = mean(sst)) %>% ungroup()

D_sst <- read_csv("SST_data_sounds.csv") %>% mutate(sst =  sst*(9/5)+32)

D_sst<-tidync("sst.mean.nc") %>% # all sounds are in this grid cell
  hyper_filter(lat = between(lat, 31, 32), lon = between(lon, -81+360,-80+360)) %>% hyper_tibble %>% 
  mutate(date_obj = as_date(time, origin = "1800-01-01")) %>% 
  mutate(sst=sst*(9/5)+32,year=year(date_obj), month = month(date_obj)) %>% select(year, month, date_obj, sst)

baseline_1951_1980<-D_sst%>% filter(between(year, 1951,1980)) %>% group_by(month) %>% 
  summarise(sst_base_sst = mean(sst, na.rm = TRUE)) 

baseline_1951_1980_dnr<-D_dnr%>% filter(between(year, 1951,1982)) %>% group_by(month) %>% 
  summarise(sst_base_dnr = mean(sst))

baseline_1951_1980<-baseline_1951_1980 %>% left_join(baseline_1951_1980_dnr, by = "month") %>% 
  mutate(base_correction=sst_base_sst-sst_base_dnr) %>% select(month, sst_base_sst, base_correction)

D_sst_f <- D_dnr %>% left_join(baseline_1951_1980) %>% 
  mutate(sst_a = sst-sst_base_sst, source = "sounds", year = year %>% as.numeric()) %>% group_by(year, month) %>% 
  summarise(source = source[1],sst_a = mean(sst_a), sst = mean(sst)) %>% select(source, year, month, sst_a)

D_temps_essrt<-bind_rows(D_sst_decadal_f, D_sst_f) %>% mutate(
  date_obj = parse_date_time(str_c(month,"-",year), "m!*-Y!"), 
  year=year %>% as.numeric(),
  era = case_when(
    year %>% between(1970, 1979)~"70s",
    year %>% between(1980, 1989)~"80s",
    year %>% between(1990, 1999)~"90s",
    year %>% between(2000, 2009)~"00s",
    year %>% between(2010, 2019)~"10s"),
  month = case_when(
    month=="1"~"Jan",
    month=="2"~"Feb",
    month=="3"~"Mar",
    month=="4"~"Apr",
    month=="5"~"May",
    month=="6"~"Jun",
    month=="7"~"Jul",
    month=="8"~"Aug",
    month=="9"~"Sept",
    month=="10"~"Oct",
    month=="11"~"Nov",
    month=="12"~"Dec") %>% 
    factor() %>% 
    fct_relevel("Jan", "Feb","Mar","Apr","May","Jun","Jul","Aug","Sept","Oct","Nov","Dec")
) %>% na.omit 

D_temps_essrt %>% filter(source == "sounds") %>%  group_by(era, month) %>%summarise(temp = mean(sst_a)) %>% 
  ungroup %>% mutate(era = era %>% fct_relevel( "70s","80s","90s","00s","10s")) %>% 
  ggplot(aes(x = month, y = temp, color=era, group = era))+geom_line(size=1.1, alpha = .75)+
  scale_color_manual(
    values = c("#d8d6dd","#a7a8c1", "#6b7fa8", "#476c9a","#045a8d"), name = "Era")+
  theme_minimal()+theme(axis.text.x = element_text(angle = 45))+xlab("Month of the year")+
  ylab("Average monthly water temperature (F)")+ggtitle("Temperatures generally higher")

global_era<-D_temps_essrt %>% filter(source == "global") %>%group_by(era, month) %>%summarise(temp = mean(sst_a)) %>%
  ungroup %>%  mutate(era = era %>% fct_relevel("50s","60s", "70s","80s","90s","00s","10s")) %>% 
  ggplot(aes(x = month, y = temp, color=era, group = era))+geom_line(size=1.1, alpha = .75)+
  scale_color_manual(
    values = c("#d8d6dd", "#c0becf","#a7a8c1","#8a93b4", "#6b7fa8", "#476c9a","#045a8d"), name = "Era")+
  theme_minimal()+theme(axis.text.x = element_text(angle = 45))+xlab("Month of the year")+
  ylab("")+ggtitle("Temperatures generally higher")

D_temps_essrt %>% filter(date_obj>ymd("1979-01-01")&source=="sounds")%>%
  ggplot(aes(x = date_obj, y = sst_a+mean_diff))+geom_line(color="#363655", alpha = .8)+theme_minimal()+
  geom_smooth()+xlab("Time")+ylab("Water temperature (F)")+ggtitle("The water's getting hotter")

global_mo<-D_temps_essrt %>% filter(date_obj>ymd("1979-01-01")&source=="global")%>%
  ggplot(aes(x = date_obj, y = sst_a))+geom_line(color="#363655", alpha = .8)+theme_minimal()+
  geom_smooth()+xlab("Time")+ylab("")

grid.arrange(sound_era, global_era, sound_mo, global_mo, ncol = 2)

